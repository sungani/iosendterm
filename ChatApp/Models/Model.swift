//
//  File.swift
//  ChatApp
//
//  Created by Saniya Gani on 02.03.2021.
//

import Foundation


struct MessageEntity {
    var message : String
    var sender : String
}
