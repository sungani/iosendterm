//
//  LoginViewController.swift
//  ChatApp
//
//  Created by Saniya Gani on 02.03.2021.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginButton.layer.cornerRadius = 10
        loginButton.layer.masksToBounds = true
    }
    

    @IBAction func didTaploginButton(_ sender: Any) {
        guard let email = emailTextField.text else {return}
        guard let pass = passwordTextField.text else {return}
        
        Auth.auth().signIn(withEmail: email, password: pass) { (user, error) in
            if error != nil{
                print("error login", error!)
            } else{
                self.performSegue(withIdentifier: "loginGoChatPage", sender: self)
            }
        }
    }
    

}
