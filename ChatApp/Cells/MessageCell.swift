//
//  MessageCell.swift
//  ChatApp
//
//  Created by Saniya Gani on 02.03.2021.
//

import UIKit

class MessageCell: UITableViewCell {
    
    public static let identifier = String(describing: MessageCell.self)
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var senderNameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.containerView.layer.cornerRadius = 7
        containerView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
