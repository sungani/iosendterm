//
//  RegisterViewController.swift
//  ChatApp
//
//  Created by Saniya Gani on 02.03.2021.
//

import UIKit
import Firebase

class RegisterViewController: UIViewController {
    
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        registerButton.layer.cornerRadius = 10
        registerButton.layer.masksToBounds = true
    }
    

    @IBAction func didTapRegisterButton(_ sender: Any) {
        guard let email = emailTextField.text else {return}
        guard let pass = passwordTextField.text else {return}
        Auth.auth().createUser(withEmail: email, password: pass) { (user, error) in
            if error != nil{
                print("error creation", error!)
            } else{
                self.performSegue(withIdentifier: "registrationGoChatPage", sender: self)
                
            }
        }
    }
    

}
