//
//  ChatViewController.swift
//  ChatApp
//
//  Created by Saniya Gani on 02.03.2021.
//

import UIKit
import Firebase

class ChatViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var hightConstraint: NSLayoutConstraint!
    
    private var messagesArray : [MessageEntity] = []
    private let messageDB = Database.database().reference().child("Messages")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(moveBackTextField))
        tableView.addGestureRecognizer(tapGesture)
        tableView.register(UINib(nibName: MessageCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: MessageCell.identifier)
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        inputTextField.delegate = self
        tableView.estimatedRowHeight = 120
        sendButton.layer.cornerRadius = 5
        sendButton.layer.masksToBounds = true
        retrieveMessages()
    }
}


extension ChatViewController : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.hightConstraint.constant = 300 + self.hightConstraint.constant
            self.view.layoutIfNeeded()
        }
        scrollToLastMessage()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.hightConstraint.constant = self.hightConstraint.constant - 50
            self.view.layoutIfNeeded()
        }
        scrollToLastMessage()
    }
}

extension ChatViewController {
    private func retrieveMessages(){
        let messageDB = Database.database().reference().child("Messages")
        messageDB.observe(.childAdded) { (snapshot) in
            let snapshotValue = snapshot.value as? [String : String]
            guard let text = snapshotValue?["message"] else { return }
            guard let sender = snapshotValue?["sender"] else { return }
            
            
            self.messagesArray.append(MessageEntity(sender: sender, message: text))
            self.tableView.reloadData()
        }
    }
    
    @IBAction func didTapSendButton(_ sender: Any) {
        guard let email = Auth.auth().currentUser?.email else {return}
        guard let message = inputTextField.text else {return}

        let messagesDict = ["sender": email , "message" : message]
        
        sendButton.isEnabled = false

        messageDB.childByAutoId().setValue(messagesDict) { (error, reference) in
            if error != nil{
                print("Error sending", error!)
            }
            else{
                self.sendButton.isEnabled = true
                self.inputTextField.text = ""
                self.scrollToLastMessage()
            }
        }
    }
    
    @IBAction func didTapLogOutButton(_ sender: Any) {
        do {
            try Auth.auth().signOut()
            self.navigationController?.popToRootViewController(animated: true)
        }catch{
            print(error)
            
        }
    }
}



extension ChatViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messagesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MessageCell.identifier, for: indexPath) as! MessageCell
        let mess =  messagesArray[indexPath.row]
        cell.senderNameLabel.text = mess.sender
        cell.messageLabel.text = mess.message
        
        if mess.sender == Auth.auth().currentUser?.email{
            cell.containerView.backgroundColor = .systemPink
        }
        else{
            cell.containerView.backgroundColor = .systemBlue
        }
        return cell
    }
}


extension ChatViewController{
    private func scrollToLastMessage(){
        let indexPath = IndexPath(row: messagesArray.count - 1, section: 0)
        tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
    
    @objc func moveBackTextField(){
        inputTextField.endEditing(true)
    }
}
